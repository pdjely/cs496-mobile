package com.pe.cookbook;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.annotation.Target;
import java.net.URISyntaxException;

public class MainActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_view);
    }

    /**
     * Click handler to launch the view recipes activity
     * @param view
     */
    public void viewRecipes(View view) {
        Intent viewIntent = new Intent(this, ViewRecipesActivity.class);
        startActivity(viewIntent);
    }

    /**
     * Click handler to launch the add recipe activity
     * @param view
     */
    public void addRecipe(View view) {
        Intent addIntent = new Intent(this, AddRecipesActivity.class);
        startActivity(addIntent);
    }

    public void test_method(View view) {
        // Disable the main thread networking exception for testing purposes. This is just to avoid
        // dealing with threading while testing
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        DatabaseConnection connection = new DatabaseConnection();
        try {
            JSONArray response = connection.getUriJsonArray("/recipes");
            Log.d("Main", response.toString());
        }
        catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
