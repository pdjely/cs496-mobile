package com.pe.cookbook;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.AvoidXfermode;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by paul on 2/13/15.
 */
public class ViewRecipesActivity extends ListActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewrecipes_view);

        // FIXME: allow retrieval on main thread for testing
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // Retrieve the list of all recipes in the database
        // A real system would limit the search results or start with
        // a smaller query
        JSONArray recipesJson = null;
        ArrayList<ModelRecipe> recipes = null;
        try {
            recipesJson = DatabaseConnection.getUriJsonArray("/recipes");
            recipes = ModelRecipe.fromJSON(recipesJson);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Take our data and display in the listview. Just use the recipe title as the
        // list item description
        ViewAllAdapter viewAdapter = new ViewAllAdapter(this, recipes);
        setListAdapter(viewAdapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        ListView lv = getListView();
        ModelRecipe item = (ModelRecipe) lv.getAdapter().getItem(position);

        Toast msg = Toast.makeText(this, item.getName(), Toast.LENGTH_LONG);
        msg.show();

        // Launch the detail view with an intent. Attach the array item for the current row to the intent
        Intent viewIntent = new Intent(this, ViewDetailActivity.class);
        try {
            viewIntent.putExtra("json", item.toJsonString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        startActivity(viewIntent);
    }

    private class ViewAllAdapter extends ArrayAdapter<ModelRecipe> {

        public ViewAllAdapter(Context context, ArrayList<ModelRecipe> recipes) {
            super(context, 0, recipes);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get reference to the inflater and then inflate the interface for this row item
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.viewrecipes_list_layout, parent, false);

            // Load the item at the current position for this row
            ModelRecipe currentItem = getItem(position);

            TextView t = (TextView) row.findViewById(R.id.viewRecipeTitle);
            t.setText(currentItem.getName());

            // TODO: add picture to thumbnail box

            return row;
        }
    }

}
