package com.pe.cookbook;

import android.content.Context;
import android.widget.TableRow;
import android.widget.TextView;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by paul on 2/14/15.
 */
public class ModelIngredient {
    private String ingredientName;
    private Double amount;
    private String unit;

    public ModelIngredient() {
        ingredientName = "";
        amount = 0.0;
        unit = "";
    }

    public ModelIngredient(String n, Double a, String u) {
        ingredientName = n;
        amount = a;
        unit = u;
    }

    /**
     * Convert the data in the object to a TableRow for display
     * @return
     */
    public TableRow toTableRow(Context context) {
        TableRow row = new TableRow(context);

        TextView n = new TextView(context);
        TextView a = new TextView(context);
        TextView u = new TextView(context);

        n.setText(ingredientName);
        a.setText(amount.toString());
        u.setText(unit);

        row.addView(n);
        row.addView(a);
        row.addView(u);

        return row;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("ingredient", ingredientName);
        json.put("amount", amount);
        json.put("unit", unit);

        return json;
    }

    public String getIngredient() {
        return ingredientName;
    }

    public Double getAmount() {
        return amount;
    }

    public String getUnit() {
        return unit;
    }

    public void setIngredientName(String ingredientName) {
        this.ingredientName = ingredientName;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
