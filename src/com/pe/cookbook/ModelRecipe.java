package com.pe.cookbook;

import android.graphics.AvoidXfermode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by paul on 2/14/15.
 */
public class ModelRecipe {
    private String name;
    private String slug;
    private ArrayList<ModelIngredient> ingredients;
    private ArrayList<String> tags;
    private ArrayList<String> directions;
    private String notes;
    private JSONArray events;   //always empty
    private String jsonString;  // for serializing and attaching to intents
    // Events are null for this
    private String encodedImage;

    /////////// Factory Methods ///////////////////////////////////////////////////////////////////////////////////////

    /**
     * Factory method to instantiate a new ModelRecipe object from a JSON object (singular)
     *
     * @param json json-object
     * @return new ModelRecipe object
     * @throws JSONException parse errors need to be caught by the caller
     */
    public static ModelRecipe fromJSON(JSONObject json) throws JSONException {
        // Construct a new ModelRecipe object and then add pieces one by one
        ModelRecipe model = new ModelRecipe();

        model.setName(json.getString("name"));
        model.setSlug(json.getString("slug"));

        JSONArray tagsArray = json.getJSONArray("tags");
        for (int i=0; i < tagsArray.length(); i++) {
            model.addTag(tagsArray.getString(i));
        }

        JSONArray ingredientsArray = json.getJSONArray("ingredients");
        for (int i=0; i< ingredientsArray.length(); i++) {
            ModelIngredient m = new ModelIngredient();
            JSONObject obj = ingredientsArray.getJSONObject(i);
            m.setIngredientName(obj.getString("ingredient"));
            m.setAmount(obj.getDouble("amount"));
            m.setUnit(obj.getString("unit"));

            model.addIngredient(m);
        }

        JSONArray directionsArray = json.getJSONArray("directions");
        for (int i=0; i<directionsArray.length(); i++) {
            model.addDirection(directionsArray.getString(i));
        }

        model.setNotes(json.getString("notes"));

        // Pull the encoded image string if it there
        if (json.has("photo")) {
            model.setEncodedImage(json.getString("photo"));
        }

        // Save the original json string so we can just use that to attach to intents
        // This needs to be optimized. Reserializing will be slow if there are big images.
        model.jsonString = json.toString();

        return model;
    }

    /**
     * Factory method to generate an ArrayList of ModelRecipe objects from a JSON array
     *
     * @param jsonArray array of json objects
     * @return ArrayList of modelrecipes
     */
    public static ArrayList<ModelRecipe> fromJSON(JSONArray jsonArray) throws JSONException {
        ArrayList<ModelRecipe> recipes = new ArrayList<ModelRecipe>();
        for (int i=0; i<jsonArray.length(); i++) {
            recipes.add(ModelRecipe.fromJSON(jsonArray.getJSONObject(i)));
        }

        return recipes;
    }

    /**
     * Default constructor. Just builds an empty object
     */
    public ModelRecipe() {
        name = "";
        ingredients = new ArrayList<ModelIngredient>();
        tags = new ArrayList<String>();
        directions = new ArrayList<String>();
        notes = "";
        events = new JSONArray();
        encodedImage = null;
    }

    /**
     * Constructor.
     *
     * @param name
     * @param ingredients
     * @param tags
     * @param directions
     * @param notes
     */
    public ModelRecipe(String name, String slug, ArrayList<String> tags,
                       ArrayList<ModelIngredient> ingredients,
                       ArrayList<String> directions, String notes) {
        this.name = name;
        this.slug = slug;
        this.ingredients = ingredients;
        this.tags = tags;
        this.directions = directions;
        this.notes = notes;
        events = new JSONArray();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
    // Auto-generated getters/setters

    public String toJsonString() throws JSONException {
        if (jsonString != null && jsonString != "")
            return jsonString;
        else {
            // Need to serialize from data in the object
            JSONObject json = new JSONObject();
            json.put("name", name);
            json.put("slug", slug);

            // Builds tags JSON array from string array
            json.put("tags", new JSONArray(tags));

            // Build ingreients JSON array from ingredients array
            JSONArray ingredientsArray = new JSONArray();
            for (ModelIngredient m : ingredients) {
                ingredientsArray.put(m.toJSON());
            }
            json.put("ingredients", ingredientsArray);

            // Build directions array from directions
            json.put("directions", new JSONArray(directions));

            json.put("notes", notes);
            json.put("events", events);

            if (encodedImage != null) {
                json.put("photo", encodedImage);
            }

            return json.toString();
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public ArrayList<ModelIngredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(ArrayList<ModelIngredient> ingredients) {
        this.ingredients = ingredients;
    }

    public void addIngredient(ModelIngredient i) {
        ingredients.add(i);
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    public void addTag(String t) {
        tags.add(t);
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public ArrayList<String> getDirections() {
        return directions;
    }

    public void setDirections(ArrayList<String> directions) {
        this.directions = directions;
    }

    public void addDirection(String d) {
        directions.add(d);
    }

    public void setEncodedImage(String encodedImage) {
        this.encodedImage = encodedImage;
    }

    public String getEncodedImage() {
        return encodedImage;
    }
}
