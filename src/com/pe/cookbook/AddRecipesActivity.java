package com.pe.cookbook;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.*;
import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by paul on 2/14/15.
 *
 */
public class AddRecipesActivity extends Activity {
    private ArrayList<ModelIngredient> ingredientsArray;
    private IngredientsAdapter ingredientsAdapter;
    private ImageView thumbnail;
    private String encodedImage;
    private ProgressDialog pd;
    private Context context;

    /**
     * Create a new activity. Sets up the initial ingredients and instructions
     * array and populates them with an initial empty item.
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addrecipes_view);

        context = this;

        thumbnail = (ImageView) findViewById(R.id.addrecipe_photo);
        encodedImage = null;

        // Set the ingredients list view up with an adaptor and fill with the ingredients array
        ingredientsArray = new ArrayList<ModelIngredient>();
        ingredientsAdapter = new IngredientsAdapter(this, ingredientsArray);

        // Add the adapter to the ingredients list view
        ListView ingredientsList = (ListView) findViewById(R.id.ingredients_list);
        ingredientsList.setAdapter(ingredientsAdapter);

        // Just populate with one empty object
        ModelIngredient i = new ModelIngredient();
        ingredientsAdapter.add(i);
    }

    public void clickAddIngredientBtn(View view) {
        ModelIngredient i = new ModelIngredient();
        ingredientsArray.add(i);
        ingredientsAdapter.notifyDataSetChanged();
    }

    public void clickAddRecipeBtn(View view) throws JSONException {
        Toast msg = Toast.makeText(this, "Preparing recipe for transmission", Toast.LENGTH_LONG);
        msg.show();

        // Create a ModelRecipe object from the form data
        EditText nameText = (EditText) findViewById(R.id.addrecipe_name);
        String name = nameText.getText().toString();

        EditText slugText = (EditText) findViewById(R.id.addrecipe_slug);
        String slug = slugText.getText().toString();

        EditText tagString = (EditText) findViewById(R.id.addrecipe_tags);
        ArrayList<String> tags = new ArrayList<String>(Arrays.asList(tagString.getText().toString().split("\\s*,\\s*")));

        // pull out each row and make a ModelIngredient
        ListView ingredientsList = (ListView) findViewById(R.id.ingredients_list);
        ArrayList<ModelIngredient> ingredientsModelList = new ArrayList<ModelIngredient>();
        for (int i=0; i < ingredientsList.getAdapter().getCount(); i++) {
            View v = ingredientsList.getChildAt(i);
            EditText ing = (EditText) v.findViewById(R.id.ingredientLayoutName);
            EditText a = (EditText) v.findViewById(R.id.ingredientLayoutQuantity);
            EditText u = (EditText) v.findViewById(R.id.ingredientLayoutUnit);

            ModelIngredient mi = new ModelIngredient(ing.getText().toString(), Double.valueOf(a.getText().toString()), u.getText().toString());
            ingredientsModelList.add(mi);
        }

        EditText directionsText = (EditText) findViewById(R.id.addrecipe_instructions);
        ArrayList<String> directions = new ArrayList<String>();
        directions.add(directionsText.getText().toString());

        // Now make the actual ModelRecipe object so it can be serialized
        ModelRecipe recipe = new ModelRecipe(
                name,
                slug,
                tags,
                ingredientsModelList,
                directions,
                ""
        );
        //Check if an image is present and if so use it
        if (encodedImage != null) {
            recipe.setEncodedImage(encodedImage);
            Log.d("image", encodedImage);
        }

        String json = recipe.toJsonString();

        Log.d("Serialized recipe", json);

        // POST it
        new AsyncPost().execute(json);
    }

    /**
     * Launch the camera. Take picture and then return result and display in the mini image frame
     * @param view
     */
    public void clickAddPhotoBtn(View view) {
        Intent shootIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(shootIntent, 0);
    }

    /**
     * Callback method that is called after user is finished taking the picture with the camera.
     * Once we have the picture, we display it in the imageview at the bottom of the screen.
     *
     * @param requestCode unique id for this request
     * @param resultCode result of the request
     * @param data Intent holding data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_CANCELED) {
            return;
        }

        // Retrieves the image from the camera
        Bitmap image = (Bitmap) data.getExtras().get("data");
        thumbnail.setImageBitmap(image);

        // Try to POST it to mongo
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, out);
        byte[] rawData = out.toByteArray();
        String imageBase64 = Base64.encodeToString(rawData, Base64.DEFAULT);
        encodedImage = imageBase64;
    }


    ///// AsyncPOST class /////////
    private class AsyncPost extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... params) {
            boolean success = false;
            try {
                success = DatabaseConnection.postJsonUri("/recipes", params[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (success) {
                return "Saved successfully";
            }
            else {
                return "Error saving";
            }
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(context);
            pd.setTitle("Saving recipe to database...");
            pd.setMessage("processing...");
            pd.setCancelable(false);
            pd.setIndeterminate(true);
            pd.show();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("AsyncPost", result);
            if (pd != null) {
                pd.dismiss();
                Toast msg = Toast.makeText(context, result, Toast.LENGTH_SHORT);
                msg.show();
            }
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
        }
    }
}