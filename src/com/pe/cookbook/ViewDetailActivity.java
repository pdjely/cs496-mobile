package com.pe.cookbook;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.*;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by paul on 2/15/15.
 */
public class ViewDetailActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_recipe_details);

        Intent intent = getIntent();
        ModelRecipe model = null;

        if (intent != null) {
            String jsonString = intent.getStringExtra("json");
            try {
                JSONObject json = new JSONObject(jsonString);
                model = ModelRecipe.fromJSON(json);
            } catch (JSONException e) {
                e.printStackTrace();
                Toast msg = Toast.makeText(this, "Error de-serializing from json: " + e.getMessage(), Toast.LENGTH_LONG);
                msg.show();
                finish();
            }

            // Now set all the fields in the view
            TextView name = (TextView) findViewById(R.id.detailRecipeName);
            name.setText(model.getName());

            TextView slug = (TextView) findViewById(R.id.detailRecipeSlug);
            slug.setText("Slug: " + model.getSlug());

            // Set up imageview -- base64 decode value and fill image box
            if (model.getEncodedImage() != null) {
                byte[] decodedImage = Base64.decode(model.getEncodedImage(), Base64.DEFAULT);
                Bitmap decodedBytes = BitmapFactory.decodeByteArray(decodedImage, 0, decodedImage.length);
                ImageView iv = (ImageView) findViewById(R.id.detailImage);
                iv.setImageBitmap(decodedBytes);
            }


            TextView tags = (TextView) findViewById(R.id.detailRecipeTags);
            StringBuilder sb = new StringBuilder();
            sb.append("Tags: ");
            for (String s : model.getTags()) {
                sb.append(s).append(",");
            }
            sb.setLength(sb.length() - 1);
            tags.setText(sb.toString());

            TableLayout ingredientsTable = (TableLayout) findViewById(R.id.detailIngredientsTable);
            ArrayList<ModelIngredient> il = model.getIngredients();
            TableLayout.LayoutParams trParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT);
            trParams.setMargins(10, 2, 10, 2);
            TableRow th = (TableRow) findViewById(R.id.detailIngredientTableHeader);
            th.setLayoutParams(trParams);

            for (ModelIngredient i : il) {
                TableRow r = i.toTableRow(this);
                ingredientsTable.addView(r, trParams);
            }

            TextView directions = (TextView) findViewById(R.id.detailRecipeDirections);
            sb.setLength(0);
            int counter = 1;
            for (String s : model.getDirections()) {
                Log.d("Detail direction", s);
                sb.append(counter++).append(". ").append(s).append("\n\n");
            }
            sb.setLength(sb.length()-2);
            directions.setText(sb.toString());
        }
    }
}