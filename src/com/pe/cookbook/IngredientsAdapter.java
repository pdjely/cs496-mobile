package com.pe.cookbook;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by paul on 2/14/15.
 *
 * Custom adapter and ArrayList class adapted from:
 *      https://github.com/codepath/android_guides/wiki/Using-an-ArrayAdapter-with-ListView
 *
 * References used for creating TextWatcher
 *      http://stackoverflow.com/questions/7735936/android-edittexts-within-listview-bound-to-custom-arrayadapter-keeping-track
 *      http://www.vogella.com/tutorials/AndroidListView/article.html#listsactivity
 */
public class IngredientsAdapter extends ArrayAdapter<ModelIngredient> {

    /**
     * Constructor
     */
    public IngredientsAdapter(Context context, ArrayList<ModelIngredient> ingredients) {
        super(context, 0, ingredients);
    }

    /**
     * ViewHolder class. Caches the text fields so that they aren't completely recreated
     * on screen redraw
     */
    private static class ViewHolder {
        EditText ingredient;
        EditText amount;
        EditText unit;
        TextWatcher ingredientWatcher;
        TextWatcher amountWatcher;
        TextWatcher unitWatcher;
    }

    /**
     * This method renders the list whenever it is scrolled, created, or when items are added.
     * If it is a new view, then it inflates the layout "ingredient_layout" into Java object.
     *
     * @param position position in the array we're working on
     * @param convertView current view
     * @param parent parent element
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ModelIngredient data = getItem(position);

        if (convertView == null) {
            // This creates (inflates) the view from the layout xml if it does not already exist
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.ingredient_layout, parent, false);

            // Stash references to each of the views in the view holder to improve caching efficiency
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.ingredient = (EditText) convertView.findViewById(R.id.ingredientLayoutName);
            viewHolder.amount = (EditText) convertView.findViewById(R.id.ingredientLayoutQuantity);
            viewHolder.unit = (EditText) convertView.findViewById(R.id.ingredientLayoutUnit);
            convertView.setTag(viewHolder);
        }

        // First remove the old text watchers if present
        ViewHolder oldHolder = (ViewHolder) convertView.getTag();
        if (oldHolder.ingredientWatcher != null) {
            // If it has one watcher then it has all of them
            oldHolder.ingredient.removeTextChangedListener(oldHolder.ingredientWatcher);
            oldHolder.unit.removeTextChangedListener(oldHolder.unitWatcher);
            oldHolder.amount.removeTextChangedListener(oldHolder.amountWatcher);
        }

        // Add text watchers to each of the fields
        oldHolder.ingredientWatcher = new IngredientTextWatcher(oldHolder.ingredient, data);
        oldHolder.amountWatcher = new IngredientTextWatcher(oldHolder.amount, data);
        oldHolder.unitWatcher = new IngredientTextWatcher(oldHolder.unit, data);

        oldHolder.ingredient.addTextChangedListener(oldHolder.ingredientWatcher);
        oldHolder.amount.addTextChangedListener(oldHolder.amountWatcher);
        oldHolder.unit.addTextChangedListener(oldHolder.unitWatcher);

        oldHolder.ingredient.setText(data.getIngredient());
        oldHolder.amount.setText(data.getAmount().toString());
        oldHolder.unit.setText(data.getUnit());

        return convertView;
    }

    /**
     * Custom text watcher. It watches all of the columns in the ingredients row for changes.
     * When changes are made then the data is saved to the in-memory array (no persistence)
     */
    private class IngredientTextWatcher implements TextWatcher {
        private EditText field;             // textfield being watched
        private ModelIngredient item;       // array value that the field belongs to

        /**
         * This constructor is invoked for each field in the ingredients row of the list view that is added
         * to the add recipes activity. It binds a watcher to each field.
         * @param f field to watch
         * @param i ModelIngredient object that holds the field (and other fields in the same row)
         */
        public IngredientTextWatcher(EditText f, ModelIngredient i) {
            field = f;
            item = i;
        }

        @Override
        public void afterTextChanged(Editable data) {
            // Find out which field was changed and then use the setter method to update the object
            // that is in the array
            switch (field.getId()) {
                case R.id.ingredientLayoutName:
                    item.setIngredientName(data.toString());
                    break;
                case R.id.ingredientLayoutQuantity:
                    item.setAmount(Double.parseDouble(data.toString()));
                    break;
                case R.id.ingredientLayoutUnit:
                    item.setUnit(data.toString());
                    break;
            }
        }

        @Override
        public void beforeTextChanged (CharSequence s, int start, int count, int after) { }
        @Override
        public void onTextChanged (CharSequence s, int start, int before, int count) { }
    }

}
