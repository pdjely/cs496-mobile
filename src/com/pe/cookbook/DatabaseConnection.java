package com.pe.cookbook;

import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * This class handles all HTTP requests to the backend database. It makes the requests and deserializes
 * the JSON responses into a ModelRecipe object.
 *
 *
 * Code based on information from:
 *      Phillips, B. and Hardy, B. _Android Programming: The Big Nerd Ranch Guide_.
 *      Big Nerd Ranch Guides: 2013. Chapter 26.
 *
 *      http://stackoverflow.com/questions/9605913/how-to-parse-json-in-android
 *
 */
public class DatabaseConnection {
    private static final String logtag = "DBConnect";
    private static final String baseUri = "http://54.149.88.230:8080/api";
    private static final String baseUriTesting = "http://192.168.1.140:3000/api";

    public DatabaseConnection() {
    }

    ////////////////// CONVENIENCE METHODS /////////////////////////////////////////////////////////////////////////////

    /**
     * GET a JSON array from the specified endpoint. This will fail if the server returns an object
     * instead of an array.
     *
     * @param uriString
     * @return
     * @throws IOException
     * @throws JSONException
     */
    public static JSONArray getUriJsonArray(String uriString) throws IOException, JSONException {
        String response = getUriString(uriString);

        try {
            JSONArray json = new JSONArray(response);
            Log.d("Main", json.toString());
            return json;
        }
        catch (JSONException e) {
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * Retrieves a JSON Object (single data value) from the server. This will fail if it
     * gets an array value.
     *
     * @param uriString endpoint to get data from
     * @return JSONObject
     * @throws IOException communication error with server
     * @throws JSONException error parsing response into json
     */
    public static JSONObject getUriJson(String uriString) throws IOException, JSONException {
        String response = getUriString(uriString);

        try {
            JSONObject json = new JSONObject(response);
            Log.d("Main", json.toString());
            return json;
        }
        catch (JSONException e) {
            e.printStackTrace();
            throw e;
        }
    }


    public static boolean postJsonUri(String uriString, String json) throws IOException {
        URL url = new URL(baseUri + uriString);

        // Try to do a GET on the given URI with expected content-type of application/json
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        try {
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setFixedLengthStreamingMode(json.getBytes().length);
            conn.setRequestProperty("Content-type", "application/json");

            Log.d("Post", "Target url: " + url.toString());

            conn.connect();
            Log.d("POST", json);

            // Send the json string
            OutputStream out = new BufferedOutputStream(conn.getOutputStream());
            out.write(json.getBytes());
            out.flush();

            // Read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
            StringBuilder sb = new StringBuilder(4096*10);
            String s;
            while((s = reader.readLine()) != null) {
                sb.append(s);
            }
            Log.d("POST response", sb.toString());

            if (conn.getResponseCode() >= 200 && conn.getResponseCode() < 300) {
                return true;
            }
            else {
                return false;
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            conn.disconnect();
        }

        return false;
    }

    /**
     * Makes a GET request to the supplied URI. Reads the entire response into a string and
     * returns the string. This is the lowest-level internal helper method.
     *
     * @param uriString full URI to resource to GET
     * @return String containing response
     * @throws IOException
     */
    private static String getUriString(String uriString) throws IOException {
        URL url = new URL(baseUri + uriString);

        // Try to do a GET on the given URI with expected content-type of application/json
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.connect();
        int response = conn.getResponseCode();

        Log.d(logtag, response + ": Opened connection with " + uriString);

        // Read data from the URL into a string
        try {
            InputStream in = new BufferedInputStream(conn.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
            StringBuilder sb = new StringBuilder(4096*10);
            String s;
            while((s = reader.readLine()) != null) {
                sb.append(s);
            }

            Log.d(logtag, "GET response: " + sb.toString());

            return sb.toString();
        }
        catch (IOException e) {
            Log.e(logtag, "Error on GET " + uriString + ": " + e.toString());
            e.printStackTrace();
            throw e;
        }
    }
}
